// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const userController = require("../controllers/userController");
// Export auth for user access


// Create a User
router.post("/register", (req, res)=>{
	userController.createUser(req.body)
	.then(userCreated =>
		res.send(userCreated))
})

// Get all users
router.get("/all", (req, res)=>{
	userController.getAllUsers()
	.then(result =>{
		res.send(result);
	})
})

// Create an Admin User
router.put("/:userId/status-updated", (req, res)=>{
	userController.makeUserAdmin(req.params)
	.then(result =>{
		res.send(result)
	})
})

// Create a User Authentication upon Login
router.post("/login", (req, res)=>{
	userController.customerLogin(req.body)
	.then(result=>{
		res.send(result)
	})
})


// Create Order Non-Admin User
router.post("/successful", (req, res)=>{
	userController.userOrder(req.body)
	.then(orders =>
		res.send(orders))
})

module.exports = router;