// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the product controller
const productController = require("../controllers/productController");

//Export auth.js
const auth = require("../auth");

// Creating a product using an Admin User							
router.post("/create", auth.verify, (req, res)=>{

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body
	}
	console.log(data)
	productController.createProduct(data)
	.then(result=>{
		res.send(result)
	})
})

// Retrieve All Products
router.get("/all", (req, res)=>{
	productController.getAllProducts()
	.then(result=>{
		res.send(result)
	})
})

// Retrieve a Single Product
router.get("/:productId", (req, res)=>{
	productController.specificProduct(req.params)
	.then(result=>{
		res.send(result)
	})
})

// Update Product Information using an Admin User
router.put("/:productId/updated-successfully", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
		console.log(data)

		if(data.isAdmin){
			productController.updateProduct(req.params, req.body)
			.then(result=>{
				res.send(result)
			})
		}else{
			res.send(false)
		}
})

// Archiving a Product using an Admin User
router.put("/:productId/archived-successfully", auth.verify, (req, res)=>{
	let adminAccess = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
		console.log(adminAccess)

		if(adminAccess.isAdmin){
			productController.archiveProduct(req.params)
			.then(result=>{
				res.send(result)
			})
		}else{
			res.send(false)
		}

})

module.exports = router;