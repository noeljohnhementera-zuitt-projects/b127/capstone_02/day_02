// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const orderController = require("../controllers/orderController");

// Export auth.js for user verification
const auth = require("../auth");

// Add customer with order (Non-Admin User)
router.post("/addCustomer", (req, res)=>{
	orderController.addCustomer(req.body)
	.then(result =>
		res.send(result))
})

// Track single customers order (Non-Admin User)
router.get("/track-order/:id", (req, res)=>{
	orderController.trackOrder(req.params.id)
	.then(result =>
		res.send(result))
})

// Get all customer orders (Non-Admin User)
router.get("/all", (req, res)=>{
	orderController.getAllOrders()
	.then(result=>{
		res.send(result)
	})
})

// Get Authenticated User's orders
router.post("/verifiedUserOrder", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body
	}
	console.log(data)

		if(data.isAdmin){
			orderController.getverifiedUserOrder(data)
			.then(result=>{
				res.send(result)
			})
		}else{
			res.send(false)
		}
	
})

/*// Create a User Order
router.post("/successful", (req, res)=>{
	let data = {
		userId: req.body.userId
		//productId: req.body.productId,
		//price: req.body.price
	}
	console.log(data)
	orderController.successfulOrder(data, req.params.id)
	.then(result =>
		res.send(result))
})*/


module.exports = router;