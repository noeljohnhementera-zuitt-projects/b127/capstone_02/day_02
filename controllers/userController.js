// Export the schema models
const User = require("../models/User");

// Add Product and Order later

// Add bcrypt and auth later 
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register / create a user
module.exports.createUser = (enterDetails) =>{
	let newUser = new User ({
		firstName: enterDetails.firstName,
		lastName: enterDetails.lastName,
		email: enterDetails.email,
		password: bcrypt.hashSync(enterDetails.password, 10) 		// I will use hash to cover the password
	})
	return newUser.save()
	.then((result, error)=>{
		if(error){
			return false;
		}else{
			return result
		}
	})
}

// Get all users
module.exports.getAllUsers = () =>{
	return User.find({})
	.then(result =>{
		return result;
	})
}

// Set User as Admin
module.exports.makeUserAdmin = (userParams)=>{

	let updatedStatus = {
		isAdmin: true
	}
	console.log(updatedStatus)
	return User.findByIdAndUpdate(userParams.userId, updatedStatus)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}

// Create a User Authentication upon Login
module.exports.customerLogin = (enterLoginDetails)=>{
	return User.findOne({email: enterLoginDetails.email})
	.then(result =>{
		if(result === null){
			return false
		}else{
			const isPasswordSame = bcrypt.compareSync(enterLoginDetails.password, result.password)
				if(isPasswordSame){
					console.log(isPasswordSame)
					return { customerAccessToken: auth.createCustomerAccessToken(result.toObject())} // gagawan ng token ang laman ng data sa auth.js
				}else{
					return false
				}
		}
	})
}