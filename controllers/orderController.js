// Export the schema models
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

// Add customer with orders (Non- Admin User)
module.exports.addCustomer = (enterUserId)=>{
	let customerOrder = new Order ({
		customerId: enterUserId._id
	})
		console.log(customerOrder)
		return customerOrder.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
}

// Track single customers order (Non-Admin User)
module.exports.trackOrder = (userId)=>{
	return Order.findById(userId)
	.then((res, err)=>{
		if(err){
			return false
		}else{
			return res
		}
	})
}

// Get all customer orders
module.exports.getAllOrders = ()=>{
	return Order.find({})
	.then(result=>{
		return result
	})
}

// Get Authenticated User's orders
module.exports.getverifiedUserOrder = (user)=> {
	if(user.isAdmin){

		let verifiedCustomer = new Order ({
		customerId: user.product._id
		})
		console.log(user)
		return verifiedCustomer.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	}else{
		return false
	}

}





module.exports.successfulOrder = async(data) =>{
	let customer = await User.findById(data.userId, orderId)
	.then(order =>{
		order.customerOrder.push({userId: data.userId})
	})
	return order.save()
}



/*module.exports.successfulOrder = async(data)=>{
	let userWhoOrder = await User.findById(data.userId)
	.then(result =>{
		result.userOrder.push({userId: data.userId})
	return result.save()
	.then((order, error)=>{
		if(error){
			return false;
		}else{
			return order
		}
	})
})
	let usersOrder = await Product.findById(data.productId)
	.then(result =>{
		result.userOrder.push({productId: data.productId})

	return result.save()
	.then((order, error)=>{
		if(error){
			return false;
		}else{
			return order
		}
	})
})

	if(userWhoOrder && usersOrder){
		return true;
	}else{
		return false;
	}

}*/